import { Post } from './post.interface';

export interface User {
  id: string;
  email: string;
  username: string;
  displayName?: string;
  emailVerified: boolean;
  photoURL?: string;

  posts: Post[]; //uid
}
