import { Heart } from './hearts.interface';

export interface Post {
  id: string;
  slug: string;
  title: string;
  content: string;
  username: string;
  heartCount: number;
  published: boolean;

  createdAt: any;
  updatedAt: any;

  uid: string; // user id
  hearts: Heart[];
}
