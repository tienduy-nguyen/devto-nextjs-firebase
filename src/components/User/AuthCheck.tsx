import Link from 'next/link';
import { useContext } from 'react';
import { UserContext } from 'src/lib/context';

export function AuthCheck(props: any) {
  const { username } = useContext(UserContext);

  return username
    ? props.children
    : props.fallback || <Link href='/enter'>You must be signed in</Link>;
}
