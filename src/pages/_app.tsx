import React from 'react';
import '../styles/globals.css';
import { Toaster } from 'react-hot-toast';
import { Navbar } from 'src/components';
import { UserContext } from 'src/lib/context';
import { useUserData } from 'src/lib/hook';

function MyApp({ Component, pageProps }) {
  const userData = useUserData();

  return (
    <UserContext.Provider value={userData}>
      <Navbar />
      <Component {...pageProps} />
      <Toaster />
    </UserContext.Provider>
  );
}

export default MyApp;
