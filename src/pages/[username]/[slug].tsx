import Link from 'next/link';
import { useDocumentData } from 'react-firebase-hooks/firestore';
import React, { useContext } from 'react';
import styles from 'src/styles/Post.module.css';
import { UserContext } from 'src/lib/context';
import { AuthCheck, HeartButton, Metatags, PostContent } from 'src/components';
import { firestore, getUserByUsername, postToJSON } from 'src/lib/firebase';

export async function getStaticProps({ params }) {
  const { username, slug } = params;
  const userDoc = await getUserByUsername(username);

  if (!userDoc) {
    return {
      notFound: true,
    };
  }

  let post;
  let path;

  const postRef = userDoc.ref.collection('posts').doc(slug);
  post = postToJSON(await postRef.get());
  path = postRef.path;

  return {
    props: { post, path },
    revalidate: 100,
  };
}

export async function getStaticPaths() {
  // Improve my using SDK to select emty docs
  const snapshot = await firestore.collectionGroup('posts').get();
  const paths = snapshot.docs.map((doc) => {
    const { slug, username } = doc.data();
    return {
      params: { username, slug },
    };
  });
  return {
    // must be in this format:
    // paths: [
    //   { params: { username, slug }}
    // ],
    paths,
    fallback: 'blocking',
  };
}

export default function Post(props) {
  const postRef = firestore.doc(props.path);
  const [realtimePost] = useDocumentData(postRef);

  const post = realtimePost || props.post;
  const { user: currentUser } = useContext(UserContext);

  return (
    <main className={styles.container}>
      <Metatags title={post.title} description={post.title} />

      <section>
        <PostContent post={post} />
      </section>

      <aside className='card'>
        <p>
          <strong>{post.heartCount || 0} 🤍</strong>
        </p>

        <AuthCheck
          fallback={
            <Link href='/enter'>
              <button>💗 Sign Up</button>
            </Link>
          }
        >
          <HeartButton postRef={postRef} />
        </AuthCheck>

        {currentUser?.uid === post.uid && (
          <Link href={`/admin/${post.slug}`}>
            <button className='btn-blue'>Edit Post</button>
          </Link>
        )}
      </aside>
    </main>
  );
}
